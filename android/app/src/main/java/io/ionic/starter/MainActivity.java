package io.ionic.starter;

import com.getcapacitor.BridgeActivity;
import android.os.Bundle;

import io.ionic.starter.plugins.customchrometab.CustomChromeTab;

public class MainActivity extends BridgeActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        registerPlugin(CustomChromeTab.class);
        super.onCreate(savedInstanceState);
    }
}
