package io.ionic.starter.plugins.customchrometab;

import android.util.Log;
import com.getcapacitor.JSObject;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.CapacitorPlugin;
import androidx.browser.customtabs.CustomTabsIntent;
import android.net.Uri;


@CapacitorPlugin(name = "CustomChromeTab")
public class CustomChromeTab extends Plugin {

    @PluginMethod()
    public void openLink(PluginCall call){
        logCallData(call);
        String url = call.getString("url");
        CustomTabsIntent intent = new CustomTabsIntent.Builder().build();
        intent.launchUrl(this.getActivity(), Uri.parse("https://" + url));
    }

    private void logCallData(PluginCall call) {
        String data = getCallDataObject(call);
        Log.i("custom-chrome-tab", "Plugin received this data from JS: " + data);
    }

    private String getCallDataObject(PluginCall call) {
        return call.getData().toString();
    }
}
