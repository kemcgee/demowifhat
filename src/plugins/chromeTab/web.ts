import { WebPlugin } from "@capacitor/core";

import type { CustomChromeTabPlugin } from "./definitions";

export class CustomChromeTabWeb
  extends WebPlugin
  implements CustomChromeTabPlugin
{
  constructor() {
    super();
  }

  async openLink(): Promise<boolean> {
    return true;
  }
}
