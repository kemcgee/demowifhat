import { registerPlugin } from "@capacitor/core";
import type { CustomChromeTabPlugin } from "./definitions";

const CustomChromeTab = registerPlugin<CustomChromeTabPlugin>(
    'CustomChromeTab', {
        web: () => import('./web').then(m => new m.CustomChromeTabWeb)
    }
)

export * from './definitions'
export { CustomChromeTab }
