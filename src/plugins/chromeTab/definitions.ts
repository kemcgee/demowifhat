export interface CustomChromeTabPlugin {
  /**
   * opens link url in custom chrome tab on Android.
   */
  openLink(args: { url: string }): Promise<boolean>;
}
