import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { CustomChromeTab } from '../plugins/chromeTab';
import ExploreContainer from '../components/ExploreContainer';
import './Tab1.css';
import { useState } from 'react';

const Tab1: React.FC = () => {
  const [url, setUrl] = useState<string | null>(null)

  const onClick = () => {
    if (url){
      CustomChromeTab.openLink({ url })
    }
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 1</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 1</IonTitle>
          </IonToolbar>
        </IonHeader>
        <ExploreContainer name="Tab 1 page" />
        <input type='url' onChange={(event) => setUrl(event.target.value)}></input>
        <button onClick={onClick}>Click me</button>
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
